﻿using System;

namespace ConsoleApp1
{
    class Program
    {
        public static void Main(string[] args)
        {
            Icalculate cal = new calculator();
            int square = cal.add(2, 1) * cal.minus(2, 1);
            Console.WriteLine(square);
        }
        class calculator : Icalculate, Iadd, Isubtract
        {
            public int minus(int a, int b)
            {
                return a - b;
            }
            public int add(int a, int b)
            {
                return a + b;
            }
        }
        interface Icalculate : Iadd, Isubtract
        {


        }
        interface Iadd
        {
            int add(int a, int b);
        }
        interface Isubtract
        {
            int minus(int a, int b);
        }
    }
}
